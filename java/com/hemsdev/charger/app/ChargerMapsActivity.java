package com.hemsdev.charger.app;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.location.*;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.*;
import com.google.gson.Gson;
import com.ocm.api.common.model.*;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class ChargerMapsActivity extends FragmentActivity {

    // UI Elements
    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private ActionBarDrawerToggle mDrawerToggle;
    private final String[] menuData ={"Map","List","Favourites","Your Account", "Settings", "About"};
    private String mTitle = "Charge Map";

    // Currently stored location
    public Location currentLocation;
    public ArrayList<ChargePoint> chargerList = new ArrayList<ChargePoint>();

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_charger_maps);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, menuData);

        final DrawerLayout drawer = (DrawerLayout)findViewById(R.id.drawer_layout);
        final ListView navList = (ListView) findViewById(R.id.left_drawer);

        navList.setAdapter(adapter);
        navList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int pos, long id) {
                drawer.setDrawerListener(new DrawerLayout.SimpleDrawerListener() {
                    @Override
                    public void onDrawerClosed(View drawerView) {
                        super.onDrawerClosed(drawerView);

                    }
                });
                drawer.closeDrawer(navList);
            }
        });
        chargerList = new ArrayList<ChargePoint>(); // reset the list (fixes memory leak)
        new GetChargePoints().execute();
        setUpMapIfNeeded();

        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                drawer,                /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer icon to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getActionBar().setTitle(mTitle);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActionBar().setTitle(mTitle);
            }
        };

        // Set the drawer toggle as the DrawerListener
        drawer.setDrawerListener(mDrawerToggle);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
            // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_search:
                // openSearch();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_charger_maps, menu);

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();

            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                mMap.setMyLocationEnabled(true);
                setUpMap();
            }
        }
    }

    private Location getMyLocation() {
        // Get location from GPS if it's available
        LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        Location myLocation = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        // Location wasn't found, check the next most accurate place for the current location
        if (myLocation == null) {
            Criteria criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_COARSE);
            // Finds a provider that matches the criteria
            String provider = lm.getBestProvider(criteria, true);
            // Use the provider to get the last known location
            myLocation = lm.getLastKnownLocation(provider);
        }

        return myLocation;
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera.
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
        currentLocation = getMyLocation();
        if (currentLocation != null) {
            CameraPosition position = new CameraPosition.Builder()
                    .target(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()))
                    .zoom(14).build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));
        }
    }

    /**
     * Async task class to get json by making HTTP call
     * */
    private class GetChargePoints extends AsyncTask<Void, Void, Void> {
        private ProgressDialog pDialog;
        // URL to get contacts JSON
        // TODO: Set to OpenChargeMap URL
        private String url = "http://api.openchargemap.io/v2/poi/?output=json&countrycode=UK&maxresults=150&latitude=" + getMyLocation().getLatitude() + "&longitude=" + getMyLocation().getLongitude();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(ChargerMapsActivity.this);
            pDialog.setMessage("Please wait, loading charge points. This may take up to a minute...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.GET);

            // Log.d("Response: ", "> " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONArray chargers = new JSONArray(jsonStr);

                    // looping through All Chargers
                    for (int i = 0; i < chargers.length(); i++) {
                        try {
                            // chargerList.add(gson.fromJson(chargers.get(i).toString(), ChargePoint.class));
                            chargerList.add(new ChargePoint(chargers.getJSONObject(i)));
                        } catch (JSONException e) {
                            Log.e("CHARGER", "Malformed gson");
                        }
                    }
                } catch (JSONException e) {
                    Log.e("CHARGER", e.toString());
                    e.printStackTrace();
                }
            } else {
                Log.e("CHARGER", "Couldn't get any data from the url");
            }
            Log.e("CHARGER", "*** Total number of charge points loaded: " + chargerList.size() + " ***");
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            String type, state, connectionTypeTitle, connectionConnector;

            for (int i=0; i<chargerList.size(); i++) {

                if (chargerList.get(i).getStatusType().getIsOperational() == "true" || chargerList.get(i).getStatusType().getIsOperational() == "") {
                    state = "_live";
                } else if (chargerList.get(i).getStatusType().getIsOperational() == "false") {
                    state = "_fault";
                } else {
                    state = "_planned";
                }

                connectionTypeTitle = connectionConnector = "Unknown";
                type = "fast";

                for (int j=0; j<chargerList.get(i).getConnections().size(); j++) {
                    switch (chargerList.get(i).getConnections().get(j).getLevel().getID()) {
                        case 1:
                            type = "fast";
                            break;
                        case 2:
                            type = "fast";
                            break;
                        case 3:
                            type = "rapid";
                            break;
                        default:
                            type = "fast";
                            break;
                    }
                    connectionTypeTitle = chargerList.get(i).getConnections().get(j).getLevel().getTitle();
                    connectionConnector = chargerList.get(i).getConnections().get(j).getConnectionType().getTitle();
                }

                int icon = getResources().getIdentifier(type + state, "drawable", "com.hemsdev.charger.app");
                mMap.addMarker(
                        new MarkerOptions()
                                .position(new LatLng(chargerList.get(i).getAddressInfo().getLatitude(), chargerList.get(i).getAddressInfo().getLongitude()))
                                .title(chargerList.get(i).getAddressInfo().getTitle())
                                .snippet(connectionTypeTitle + "(" + connectionConnector + ")")
                                .icon(BitmapDescriptorFactory.fromResource(icon))
                );

            }

            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();

            // Add to list if needs be
        }
    }
}
